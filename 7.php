<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#6</h1><p>";

    if (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
        echo "마스터키 사용 이벤트";
    } else {
        echo "정답 진행";
    }

    echo "<br><br><br>두가지 주관식 문제일 경우, 첫번째 답 | 두번째 답 포맷으로 읽어서 정답을 체크합니다. 답은 2015와 2016.</p>";
    echo "<h2>힌트</h2>";

    echo '
    <section class="content">
    <form action="checkAnswer.php" method="post">
        <span class="input input--jiro">
            <input class="input__field input__field--jiro" name="submitted_answer" type="text" id="input-10" required>
            
            <label class="input__label input__label--jiro" for="input-10">
                <span class="input__label-content input__label-content--jiro">틀린 부분</span>
            </label>
        </span>
        <span class="input input--jiro">
            <input class="input__field input__field--jiro" name="submitted_answer2" type="text" id="input-10" required>
            
            <label class="input__label input__label--jiro" for="input-10">
                <span class="input__label-content input__label-content--jiro">고친 부분</span>
            </label>
        </span>
        <input type="hidden" name="current_page" value=' .$_SESSION['current_prog'].'>
        <input type="submit" name="write" value="제출" class="input_submit"
               style="position: absolute; left: -9999px; width: 1px; height:1px;" tabindex="-1">
    </form></section>';
}