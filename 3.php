<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#2</h1><p>";

    if (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
        echo "마스터키 사용 이벤트";
    } else {
        echo "정답을 맞춰서 이동했을 경우";
    }

    echo"<br>투명 버튼 찾기 문제</p><h2>힌트</h2>";

    /**
     * 투명 버튼은 스크린 안의 랜덤한 장소에 나타남.
     * 사이즈 조절을 위해서는 각각 h와 w에서 빼는 값을 조정하세요.
     * 드래그했을 때 잘 나타나게 하기 위해서 gif 투명이미지를 사용했습니다.
     */
    echo '
<div id="random-box">
    <form action="checkAnswer.php" method="post">
        <input type="hidden" name="submitted_answer" value="continue">
        <input type="hidden" name="current_page" value='. $_SESSION['current_prog'].'>
        <input type="image" name="submit" src="img/hidden_button.gif" border="0" title="달그락" alt="달그락">
    </form>
</div>
<script>
var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
var randomBox = document.getElementById("random-box");
randomBox.style.position = "absolute";
randomBox.style.top = (Math.random() * (h - 10)).toString() + "px";
randomBox.style.left = (Math.random() * (w - 10)).toString() + "px";
//alert(w + "x" + h);
</script>';

}