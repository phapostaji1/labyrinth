<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC '-//IETF//DTD HTML 2.0//EN'>\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#11</h1><p>";

    if (isset($_SESSION['vm'])) {
        if($_SESSION['vm'] == 1) {
            echo "자판기에서 '영혼을 위한 닭고기 수프'가 나왔다.<br>내가 집기도 전에 마스터키가 달려들어 허겁지겁 먹어치워버렸다.<br>덕분에 마스터키를 한번 더 사용할수 있게된것 같긴 한데...";
        } else {
            echo "자판기에서 쿵당탕탕 거리는 소리가 나더니 모든 버튼이 고장나버렸다.<br>더 이상 아무 버튼도 눌러지지 않는 것 같다.";
        }
    } else {
        if (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
            echo "마스터키 사용 이벤트";
        } else {
            if (isset($_SESSION['special'])) {
                if ($_SESSION['special'] == 0) { // 이미 총이 있었던 경우
                    echo "이전 페이지의 특별문제 정답을 맞추었지만 이미 총을 가지고있었던 경우";
                } else {
                    echo "이전 페이지의 특별문제 정답을 맞추어서 총을 획득";
                }
            } else {
                echo "이전 페이지의 특별문제를 맞추지 않거나 틀린 답을 제출했을 경우";
            }

            echo "<br><br>이동 메시지";
        }

        echo "<br><br><br>칸에대한 설명";
    }

    echo "<br><br></p>";
    echo "<div id='vm-container'>";
    if (!isset($_SESSION['vm'])) {
        echo "
<div id='vm-btn-container'>
<div class='box'><!--63 * 58-->
    <form action='checkAnswer.php' method='post'>
        <input type='hidden' name='current_page' value=" . $_SESSION['current_prog'] . ">
        <input type='hidden' name='vm' value='1'>
        <input type='submit' name='write' value='&nbsp' class='vm-btn'>
    </form>
</div>
<div class='box'><!--63 * 58-->
    <form action='checkAnswer.php' method='post'>
        <input type='hidden' name='current_page' value=" . $_SESSION['current_prog'] . ">
        <input type='hidden' name='vm' value='2'>
        <input type='submit' name='write' value='&nbsp' class='vm-btn'>
    </form>
</div>
<div class='box'><!--63 * 58-->
    <form action='checkAnswer.php' method='post'>
        <input type='hidden' name='current_page' value=" . $_SESSION['current_prog'] . ">
        <input type='hidden' name='vm' value='3'>
        <input type='submit' name='write' value='&nbsp' class='vm-btn'>
    </form>
</div><div class='box'><!--63 * 58-->
    <form action='checkAnswer.php' method='post'>
        <input type='hidden' name='current_page' value=" . $_SESSION['current_prog'] . ">
        <input type='hidden' name='vm' value='4'>
        <input type='submit' name='write' value='&nbsp' class='vm-btn'>
    </form>
</div>
<div class='box'><!--63 * 58-->
    <form action='checkAnswer.php' method='post'>
        <input type='hidden' name='current_page' value=" . $_SESSION['current_prog'] . ">
        <input type='hidden' name='vm' value='5'>
        <input type='submit' name='write' value='&nbsp' class='vm-btn'>
    </form>
</div>
<div class='box'><!--63 * 58-->
    <form action='checkAnswer.php' method='post'>
        <input type='hidden' name='current_page' value=" . $_SESSION['current_prog'] . ">
        <input type='hidden' name='vm' value='6'>
        <input type='submit' name='write' value='&nbsp' class='vm-btn'>
    </form>
</div><div class='box'><!--63 * 58-->
    <form action='checkAnswer.php' method='post'>
        <input type='hidden' name='current_page' value=" . $_SESSION['current_prog'] . ">
        <input type='hidden' name='vm' value='7'>
        <input type='submit' name='write' value='&nbsp' class='vm-btn'>
    </form>
</div>
<div class='box'><!--63 * 58-->
    <form action='checkAnswer.php' method='post'>
        <input type='hidden' name='current_page' value=" . $_SESSION['current_prog'] . ">
        <input type='hidden' name='vm' value='8'>
        <input type='submit' name='write' value='&nbsp' class='vm-btn'>
    </form>
</div>
<div class='box'><!--63 * 58-->
    <form action='checkAnswer.php' method='post'>
        <input type='hidden' name='current_page' value=" . $_SESSION['current_prog'] . ">
        <input type='hidden' name='vm' value='9'>
        <input type='submit' name='write' value='&nbsp' class='vm-btn'>
    </form></div></div>";
    }

    echo "</div><p>이 페이지의 자판기는 또다른 퀴즈입니다. 자판기의 첫번째 줄 가장 우측 버튼을 누르면 이미 사용한 마스터키를 복구할수 있습니다. 버튼들의 사이즈 등은 css에서 일괄 조절 할수있습니다.</p>";

    include_once ("input.php");
}