<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#9</h1>";

    echo "<p><br><br><br><br><br><br><br><br><br><br><br><br>";
    if (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
        echo "마스터키 사용 이벤트";
    } else {
        echo "일반 정답 진행";
    }

    echo "<br><br><br>그림을 드래그 해서 다른곳으로 옮기면 버튼이 숨겨져있는 페이지. 일반적으로 정답을 맞추어서 진행할수도 있습니다. 가로 세로 사이즈를 css에서 적절하게 조정해주세요.<br><br></p>";
    echo "<div class='problem'><p>문제</p></div>";
    echo '<div id="btn-box"><br><br><br><form action="checkAnswer.php" method="post">
        <input type="hidden" name="red_btn" value="1">
        <input type="hidden" name="current_page" value='.$_SESSION['current_prog'].'>
        <input type="image" name="submit" src="img/red-btn.gif" border="0" alt="알수 없는 빨간 버튼이다. 눌러볼까?" title="알수 없는 빨간 버튼이다. 눌러볼까?" onmouseover="this.src=\'img/red-btn-hover.gif\';" onmouseout="this.src=\'img/red-btn.gif\';">
    </form></div>';
    echo '<div id="drag-1" class="draggable"><p></p></div>';
    echo "
<script src='js/interact.min.js'></script>
<script>
interact('.draggable')
  .draggable({
    onmove: window.dragMoveListener
  });

  function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
  }

  // this is used later in the resizing and gesture demos
  window.dragMoveListener = dragMoveListener;
  </script>";

    include_once ("input.php");
}