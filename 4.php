<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#3</h1><p>";

    if (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
        echo "마스터키 사용 이벤";
    } else {
        echo "정답 이벤트";
    }
    echo "이미지 중 하나를 택하는 문제. (이 문제 정답은 왼쪽 문)</p><h2>힌트<h2>";

    /**
     * lib 에서 이 문제의 답을 수정하기위해서는 value="left"를 다른 값으로 바꾸거나 src='img/left.gif'에서 그림경로를 변경하세요.
     * 답을 보여주려는 의도가 아니라면 value 를 answer 같은 너무 명백한 것으로 바꾸지마세요.
     */

    echo '<div class="answerBox"><div class="left">
    <form action="checkAnswer.php" method="post">
        <input type="hidden" name="submitted_answer" value="left">
        <input type="hidden" name="current_page" value=' .$_SESSION['current_prog']. '>
        <input type="image" name="submit" src="img/left.gif" border="0" alt="왼쪽 복도 끝 문" title="왼쪽 복도 끝 문">
    </form></div><div class="right">
    <form action="checkAnswer.php" method="post">
        <input type="hidden" name="submitted_answer" value="right">
        <input type="hidden" name="current_page" value=' .$_SESSION['current_prog']. '>
        <input type="image" name="submit" src="img/right.gif" border="0" alt="오른쪽 복도 끝 문" title="오른쪽 복도 끝 문">
    </form></div></div>';
}