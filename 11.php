<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#10</h1><p>";

    if (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
        echo "마스터키 사용 이벤트";
    } else {
        echo "일반 정답 진행";
    }

    echo "<br><br><br><i>이 페이지는 특별 페이지입니다.<br>문제를 맞추지 않고 아무런 단어만 써도 다음 단계로 진행할 수 있습니다.<br>정답을 맞추기 위한 기회도 단 한번 뿐입니다.</i></p>";
    echo "<h2>힌트</h2>";

    include_once ("input.php");
}