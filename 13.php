<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#기장실</h1><p>";

    if (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
        echo "마스터키 사용 이벤트";
    } else if(isset($_SESSION['red_btn'])) {
        echo "빨간 버튼 사용 이벤트";
    } else {
        echo "문제 정답 진행";
    }

    echo "<br><br><br>엔딩 전 이벤트 (문제 없이 바로 진행)<br><br><br></p>";

    echo '
    <div class="answerBox"><div class="left">
    <form action="checkAnswer.php" method="post">
        <input type="hidden" name="submitted_answer" value="continue">
        <input type="hidden" name="current_page" value=' .$_SESSION['current_prog']. '>
        <input type="submit" name="submit" class="selectCN" value="다음으로" src="img/left.gif" border="0">
    </form></div>';
}