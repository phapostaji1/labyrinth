<?php
session_start();
$session_id = session_id();

include_once ("header.html");

/** define current page */
if (!isset($_SESSION['current_prog'])) {
    $_SESSION['current_prog'] = 1;
}

// 편한 디버깅을 위해서 주소 뒤에 ?p=13 을 입력하면 그 페이지로 이동할수있는 방법입니다. 해당 옵션을 비활성화 하기 위해서 지우거나 코멘트화하세요.
if (isset($_GET['p'])) {
    $_SESSION['current_prog'] = $_GET['p'];
}

if ($_SESSION['current_prog'] > 1 && $_SESSION['current_prog'] < 13) { // 마스터키는 문제 페이지에서만 표시
    include_once ("masterKey.php");
}

include_once ($_SESSION['current_prog'].".php");
echo '<form action="checkAnswer.php" method="post">
        <input type="hidden" name="reset" value="1">
        <input type="submit" name="write" value="reset" class="input_submit">
    </form>';
include_once ("footer.html");
