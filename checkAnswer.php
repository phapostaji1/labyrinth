<?php
include_once ("lib.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(isset($_POST['reset'])) {
        session_regenerate_id();
        session_destroy();
        header('Location: index.php'); // 세션 초기화
    }

    $current_page = $_POST['current_page'];
    $submittedAnswer = textify($_POST['submitted_answer']);

    if (isset($_POST['submitted_answer2'])) { // 답이 두가지인 경우 처리 방법
        $submittedAnswer = $submittedAnswer."|".textify($_POST['submitted_answer2']);
    }

    if(isset($_POST['masterKey'])) { // 마스터키를 사용할 경우

        $_SESSION['current_prog'] = $current_page + 1; // proceed
        $_SESSION['masterKey'] = 2; // 마스터키 사용 이벤트를 보여준다.

    } else if (isset($_POST['red_btn'])) { // 빨간버튼 발견

        $_SESSION['red_btn'] = 1;
        $_SESSION['current_prog'] = sizeof($answer_key); // 바로 엔딩 전 페이지로 이동, 페이지 값을 입력해서 이동할수도 있음.

    } else if (isset($_POST['gun'])) { // 총 발견

        if ($_SESSION['gun'] < 0) {
            $_SESSION['special'] = 0;
        } else {
            $_SESSION['gun'] = -1;
            $_SESSION['special'] = 1;
        }

    } else {

        if(isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
            $_SESSION['masterKey'] = 1; // 마스터키를 이전 페이지에서 이미 사용했음.
        }

        // check answer
        if (isCorrect($answer_key[$current_page - 1], $submittedAnswer)) {
            $_SESSION['current_prog'] = $current_page + 1; // proceed

            if($current_page == 1) {

                $_SESSION['start_time'] = date('Y-m-d H:i:s'); // 시작시간
                $_SESSION['gun'] = rand(1, 100); // rand 값이 70 초과일때 총 드랍
                $_SESSION['heart'] = 5; // 시작 목숨 5개

            }

            if ($_SESSION['current_prog'] > sizeof($answer_key)) { // 엔딩 페이지

                $endingType = "True Ending"; // 로그파일에 표시할 엔딩이름을 입력할수 있음. 기본엔딩

                if (isset($_SESSION['police'])) {

                    $endingType = "The Justice"; //경찰엔딩

                } elseif (!isset($_SESSION['masterKey'])) { // 마스터키 사용하지 않음

                    $endingType .= "+"; // 마스터키 사용 하지 않음
                }

                if(!isset($_SESSION['end_time'])) { // 완료 시간
                    $_SESSION['end_time'] = date('Y-m-d H:i:s');
                }

                // 로그 파일에 입력
                enterLog(session_id(), $endingType, $_SESSION['start_time'], $_SESSION['end_time']);
            }

        } else { // 오답 케이스

            if ($current_page == 11) { // 특별문제 오답
                $endingType = "give up";
                $_SESSION['current_prog'] = $current_page + 1; // 무조건 진행
            } elseif ($current_page == 9 && $submittedAnswer == '875e') { // 9번 페이지에서 특정 오답을 입력했을경우
                $_SESSION['hex'] = 1; // 세션값을 지정해서 확인할 수 있음
            } elseif (isset($_POST['vm'])) { // 자판기 문제일 경우
                if($_POST['vm'] == 3 && isset($_SESSION['masterKey'])) {
                    $_SESSION['vm'] = 1;
                    unset($_SESSION['masterKey']);
                } else {
                    $_SESSION['vm'] = 0;
                }
            } elseif ($current_page == 6 && (strpos($submittedAnswer, '112') || $submittedAnswer == '112')) {
                $_SESSION['police'] = 1;
            } elseif ($_SESSION['heart'] > 0) {
                $_SESSION['heart'] = $_SESSION['heart'] - 1;
            } else {
                $_SESSION['current_prog'] = 0;
                $endingType = "Ending: 0";
            }
        }
    }
    header('Location: index.php');

} else { // 잘못된 경로
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
