<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "
<br>
<ul>
<li>PC 진행을 권장합니다.</li>
<li>구글 크롬에서 최적화 되었습니다. 다른 브라우저에서는 작동하지 않을 수 있습니다.</li>
<li>모든 상황은 실제와 관련이 없을 수 있습니다.</li>
<li>단 한 번, 다음 칸으로 건너갈 수 있는 마스터키가 준비되어 있습니다. 딱 한 번만 사용 가능하니 신중하게 사용하세요.</li>
<li>진행 상황은 자동으로 저장되어 한 번 탈락하면 다시 도전할 수 없습니다.</li>
<li>위 사항을 모두 숙지하셨다면 아래의 시작하기 버튼을 눌러주세요.</li>
</ul>";

    echo '
<div class="answerBox">
    <form action="checkAnswer.php" method="post">
        <input type="hidden" name="submitted_answer" class="input_friends" value="continue"></textarea>
        <input type="hidden" name="current_page" value=' .$_SESSION['current_prog']. '>
        <input type="image" name="submit" src="img/start.png" border="0" title="시작하기" alt="시작하기">
    </form>';
}