<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#5</h1><p>";

    if (isset($_SESSION['police'])) {
        echo "경찰 이벤트";
    } elseif (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
        echo "마스터키 사용 이벤트";
    } else {
        echo "정답 이벤트";
    }

    // 이런 페이지를 추가하기 위해서는 checkAnswer.php 도 함께 수정하세요.

    echo "<br><br><br>특별한 엔딩조건을 설정하기 위해서 세션값을 체크한 뒤 이벤트 문구를 보여줍니다. 이 페이지에서는 112라고 입력하면 경찰이벤트가 발동하고 이후에 경찰 엔딩을 볼수있습니다.<br><br></p>";
    echo "<div class='problem'><p>문제입력</p></div>";

    include_once ("input.php");
}