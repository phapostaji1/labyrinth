<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#7</h1><p>";

    if ($_SESSION['gun'] == -1) {
        echo "랜덤한 확률로 특수 아이템 드랍 -> 아이템 획득 이벤트</p>";
    } else {
        if (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
            echo "마스터키 사용 이벤트";
        } else {
            echo "정답 이벤트";
        }

        echo "<br>아이템 획득하기 전에 보일 문구</p>";
    }

    echo "<br><br><br><p>아이템 획득 페이지. 아이템은 엔딩에 영향을 미칠 수 있습니다. 랜덤 포지션에 이미지 버튼 형식으로 나타남.</p>";

    echo '
    <section class="content">
    <form action="checkAnswer.php" method="post">
        <span class="input input--jiro">
            <input class="input__field input__field--jiro" name="submitted_answer" type="text" id="input-10">
            
            <label class="input__label input__label--jiro" for="input-10">
                <span class="input__label-content input__label-content--jiro">정답</span>
            </label>
        </span>
        <input type="hidden" name="current_page" value=' .$_SESSION['current_prog'].'>
        <input type="submit" name="write" value="제출" class="input_submit"
               style="position: absolute; left: -9999px; width: 1px; height:1px;" tabindex="-1">
    </form></section>';

    if ($_SESSION['gun'] > 70) { // 시작할때 정해진 숫자가 70을 초과할 때 총이 드랍 됨
        echo '
<div id="random-box">
    <form action="checkAnswer.php" method="post">
        <input type="hidden" name="gun" value="1">
        <input type="hidden" name="current_page" value='.$_SESSION['current_prog'].'>
        <input type="image" name="submit" src="img/revolver.png" border="0" title="달그락" alt="달그락">
    </form>
</div>
<script>
var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
var randomBox = document.getElementById("random-box");
randomBox.style.position = "absolute";
randomBox.style.top = (Math.random() * (h - 79)).toString() + "px";
randomBox.style.left = (Math.random() * (w - 150)).toString() + "px";
//alert(w + "x" + h);
</script>';
    }
}