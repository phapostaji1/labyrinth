<?php
/** 진엔딩 */
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {

    if (isset($_SESSION['police'])) {
        echo "<h1>경찰 엔딩</h1><p>";
        echo "조건에 따라 다른 엔딩이 보일수 있습니다. 더 많은 엔딩종류를 위해서 조건문을 추가하세요. 세션값을 이용하면 됩니다.</p>";
    } else if(!isset($_SESSION['masterKey'])) {
        echo "<h1>엔딩 1</h1><p>";
        echo "조건에 따라 다른 엔딩이 보일수 있습니다. 더 많은 엔딩종류를 위해서 조건문을 추가하세요. 세션값을 이용하면 됩니다.</p>";
    } else {
        echo "<h1>엔딩 2</h1><p>";
        echo "조건에 따라 다른 엔딩이 보일수 있습니다. 더 많은 엔딩종류를 위해서 조건문을 추가하세요. 세션값을 이용하면 됩니다.</p>";
    }

    include_once ("complete.php");
}