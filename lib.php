<?php
session_start();

/**
 * 각 페이지의 정답을 순서대로 입력하세요.
 */
$answer_key = array(
    "continue", "냠냠", "continue", "left", "냠냠",
    "냠냠", "2015|2016", "냠냠", "냠냠", "냠냠",
    "냠냠", "냠냠", "continue"
);

/**
 * @param $answerkey
 * @param $subject
 * @return bool
 *
 * 정답 확인
 */
function isCorrect($answerkey, $subject) {
    return $answerkey == $subject;
}

/**
 * @param $text
 * @return mixed|string
 *
 * 사용자가 입력한 스트링에서 띄어쓰기, 대문자, 특수문자를 없애기
 */
function textify($text) {
    $text = htmlspecialchars($text, ENT_QUOTES, 'ISO-8859-1');
    $text = trim($text);
    $text = preg_replace('/\s+/', '', $text);
    $text = stripslashes($text);
    $text = strtolower($text);
    return $text;
}

/**
 * @param $sessionId
 * @param $endingType
 * @param $startTime
 * @param $endTime
 *
 * 엔딩페이지에서 세션아이디와 완료시간등을 확인하는 펑션. 정확한 시간 확인을 위해서는 타임존을 설정하면 좋습니다. 해당 로그는 그냥 logs.txt에서 확인하면 됨.
 */
function enterLog($sessionId, $endingType, $startTime, $endTime) {
    $txt = $sessionId."|".$endingType."|".$startTime."|".$endTime;
    $myfile = file_put_contents('logs.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
}