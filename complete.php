<?php

$date1 = new DateTime($_SESSION['start_time']);
$date2 = new DateTime($_SESSION['end_time']);
$interval = $date1->diff($date2);

echo "<br><br><br><br><br><br>축하합니다.<br>당신은 미궁을 성공적으로 탈출하였습니다.<br><br><br>아래의 인증코드와 완료시간을 복사한 후 부스 게시판에 인증해주세요.<br><br><br>";
echo "<strong>인증코드:</strong> ".session_id()."<br><strong>완료 시간:</strong> ".$_SESSION['end_time']."<br>";
echo "<strong>기록:</strong>".$interval->h."시간 ".$interval->i."분 ".$interval->s."초";