<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    exit("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n<html><head>\r\n<title>404 Not Found</title>\r\n</head><body>\r\n<h1>Not Found</h1>\r\n<p>The requested URL " . $_SERVER['SCRIPT_NAME'] . " was not found on this server.</p>\r\n</body></html>");
}
else {
    echo "<h1>#4</h1><p>";

    if (isset($_SESSION['masterKey']) && $_SESSION['masterKey'] > 1) {
        echo "마스터키 사용 이벤트";
    } else {
        echo "정답 이벤트";
    }

    echo "<br><br><br>일반 주관식 문제</p>";
    echo "<br><br><div class='problem'><p>쪽지 등의 특별한 텍스트는 여기에 쓸 수 있다. 스타일은 css에서 수정.</p></div>";
    echo "<h2>힌트</h2>";

    include_once ("input.php");
}